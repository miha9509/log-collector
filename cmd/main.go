package main

import (
	"logColletor/src/app"
)

func main() {
	application := app.App{}

	application.Run("./../etc/conf.yaml")
}

package app

import (
	"fmt"
	"os"
	"sync"
	"time"
)

type SyncFile struct {
	file    *os.File
	m       sync.Mutex
	sending bool
}

func newSyncFile(path string) (*SyncFile, error) {
	f, err := os.OpenFile("./files/output.txt", os.O_CREATE|os.O_APPEND|os.O_RDWR, os.ModePerm)
	if err != nil {
		fmt.Println("Unable to open file:", err)
		return nil, err
	}

	return &SyncFile{
		file: f,
	}, nil
}

func (f *SyncFile) WriteLine(line string) {
	// while sending, stop writing.
	for f.sending == true {
		time.Sleep(time.Second / 2)
	}

}

func (f *SyncFile) GetData(line string) {
	f.sending = true

	f.sending = false
}

func (f *SyncFile) Clear(line string) {

}

func (f *SyncFile) Close() {
	f.file.Close()
}

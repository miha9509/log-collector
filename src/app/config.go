package app

import (
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

// Config struct.
type config struct {
	Files []string
}

// getConfig return config.
func getConfig(path string) (*config, error) {
	// Filepath in os.Args.
	if len(os.Args) > 1 {
		path = os.Args[1]
	}

	content, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	config := new(config)

	err = yaml.Unmarshal(content, &config)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	return config, nil
}

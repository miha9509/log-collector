package app

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"logColletor/src/logtail"
	"logColletor/src/protobuf"
	"net/http"
	"strings"

	"google.golang.org/protobuf/proto"
)

type App struct{}

func (a *App) Run(confPath string) {
	logs := make(chan string, 200)

	config, err := getConfig(confPath)
	if err != nil {
		log.Fatal(err)
	}

	go logtail.FilestailListener(config.Files, logs)
	go writeInLogs(config.Files, 100)

	message := protobuf.Msg{}
	message.HostName = "mac PC"
	message.IP = "192.168.7.235"
	message.DataLog = []*protobuf.Msg_Data{}

	for i := 0; ; i++ {
		log := <-logs
		if log == "" {
			continue
		}

		log = strings.TrimSuffix(log, "\n")

		msgData := &protobuf.Msg_Data{
			Name: "name",
			Time: "time",
			Log:  log,
		}
		message.DataLog = append(message.DataLog, msgData)

		if i%10 == 0 {
			sendMessage(&message)

			message.DataLog = []*protobuf.Msg_Data{}

			fmt.Println(i)
		}
	}
}

func sendMessage(message *protobuf.Msg) error {
	msg, err := proto.Marshal(message)
	if err != nil {
		return err
	}

	fmt.Println(len(msg))
	postWithBody(msg)

	return nil
}

// ConvertToBtc Конвертация суммы валюты в биткоины.
func postWithBody(msg []byte) error {
	body := bytes.NewBuffer(msg)
	// Делаем пост запрос на blockchain.info currency=текущая валюта аккаунта value=значене валюты.
	req, err := http.NewRequestWithContext(context.Background(), http.MethodPost, "http://10.8.250.10:8080/proto", body)
	if err != nil {
		return err
	}

	client := &http.Client{}
	// Делаем запрос.
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	buf := new(bytes.Buffer)

	if _, err = buf.ReadFrom(resp.Body); err != nil {
		return err
	}

	fmt.Println(buf.String())

	return nil
}

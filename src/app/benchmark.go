package app

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"time"
)

type logstruct struct {
	time time.Time
	name string
	data string
}

func readBencmark() {
	// Timer start.
	start := time.Now()

	logArr := []logstruct{}

	f, err := os.Open("./files/output.txt")
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		logArr = append(logArr, logstruct{
			time: time.Now(),
			name: "file.txt",
			data: scanner.Text(),
		})
	}

	// Timer stop.
	fmt.Println(time.Since(start))
}

func writeInLogs(filenames []string, stringsNum int) {
	// Write in log files.
	files := []*os.File{}

	for _, name := range filenames {
		file, err := os.OpenFile(name, os.O_CREATE|os.O_APPEND|os.O_RDWR, os.ModeAppend|os.ModePerm)
		if err != nil {
			fmt.Println("Unable to open file:", err)
			return
		}
		defer file.Close()
		files = append(files, file)
	}

	for i, file := range files {
		go func(f *os.File, index int) {
			for j := 0; j < stringsNum; j++ {
				f.WriteString("hello file " + filenames[index] + "\n")
				time.Sleep(time.Millisecond)
			}
		}(file, i)
	}

	c := make(chan int)
	<-c
}

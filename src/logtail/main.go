package logtail

import (
	"bufio"
	"io"
	"log"
	"os"
	"time"
)

// https://stackoverflow.com/questions/10135738/reading-log-files-as-theyre-updated-in-go
func FiletailListener(filename string, charOffset int64, logChan chan string) {
	f, err := os.Open(filename)
	if err != nil {
		log.Println(err)
		return
	}
	defer f.Close()

	// TODO: mb change to forline.
	if charOffset > 0 {
		f.Seek(charOffset, io.SeekStart)
	}

	r := bufio.NewReader(f)
	info, err := f.Stat()
	if err != nil {
		log.Println(err)
		return
	}
	oldSize := info.Size()

	for {
		for line, _, err := r.ReadLine(); err != io.EOF; line, _, err = r.ReadLine() {
			logChan <- string(line)
		}

		pos, err := f.Seek(0, io.SeekCurrent)
		if err != nil {
			log.Println(err)
			return
		}

		for {
			time.Sleep(time.Second)
			newinfo, err := f.Stat()
			if err != nil {
				log.Println(err)
				return
			}
			newSize := newinfo.Size()
			if newSize != oldSize {
				if newSize < oldSize {
					f.Seek(0, 0)
				} else {
					f.Seek(pos, io.SeekStart)
				}
				r = bufio.NewReader(f)
				oldSize = newSize
				for line, _, err := r.ReadLine(); err != io.EOF; line, _, err = r.ReadLine() {
					logChan <- string(line)
				}

				pos, err = f.Seek(0, io.SeekCurrent)
				if err != nil {
					log.Println(err)
					return
				}
			}
		}
	}
}

type fileTail struct {
	file    *os.File
	oldSize int64
	newSize int64
	pos     int64
}

// https://stackoverflow.com/questions/10135738/reading-log-files-as-theyre-updated-in-go
func FilestailListener(filenames []string, logChan chan string) {
	tails := []*fileTail{}

	for _, name := range filenames {
		// Open file.
		f, err := os.Open(name)
		if err != nil {
			log.Println(err)
			return
		}
		// Get stat.
		info, err := f.Stat()
		if err != nil {
			log.Println(err)
			return
		}

		oldSize := info.Size()
		r := bufio.NewReader(f)

		// readFile.
		for line, _, err := r.ReadLine(); err != io.EOF; line, _, err = r.ReadLine() {
			logChan <- string(line)
		}

		pos, err := f.Seek(0, io.SeekCurrent)
		if err != nil {
			log.Println(err)
			return
		}

		tail := &fileTail{
			file:    f,
			oldSize: oldSize,
			pos:     pos,
		}

		tails = append(tails, tail)
		defer f.Close()
	}

	for {
		for i := range tails {
			newinfo, err := tails[i].file.Stat()
			if err != nil {
				log.Println(err)
				return
			}
			// TODO: newinfo get delay.
			time.Sleep(time.Millisecond * 40)
			tails[i].newSize = newinfo.Size()
			if tails[i].newSize != tails[i].oldSize {

				if tails[i].newSize < tails[i].oldSize {
					tails[i].file.Seek(0, 0)
				} else {
					tails[i].file.Seek(tails[i].pos, io.SeekStart)
				}
				reader := bufio.NewReader(tails[i].file)
				tails[i].oldSize = tails[i].newSize
				// New cycle.
				for line, _, err := reader.ReadLine(); err != io.EOF; line, _, err = reader.ReadLine() {
					logChan <- string(line)
				}
				tails[i].pos, err = tails[i].file.Seek(0, io.SeekCurrent)
				if err != nil {
					log.Println(err)
					return
				}
			}
		}
	}
}
